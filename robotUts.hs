-- spiral movement using forloop
spiral = forLoop [1..20] action
    where action = \i -> (turnRight >> moven i >> turnRight >> moven i)

-- zigzag with for loop

zigzag = forLoop [1..20] action
    where action = \i -> (moven 5 >> turnRight >> move >> turnRight
                         >> moven 5 >> turnLeft >> move >> turnLeft)

-- for loop implementation
forLoop [] act = return ()
forLoop (x:xs) act = (act x) >> (forLoop xs act)

-- mapM_ -> fungsi seperti map, tetapi digunakan untuk tipe monad
-- tipe mapM_ = mapM_ :: (Monad m, Foldable t) => (a -> m b) -> t a -> m ()