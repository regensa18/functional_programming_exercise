import Data.List

-- pythagoras
rightTriangles = [ (a, b, c) | c <- [5..], b <- [1..c-1], a <- [1..b-1], a^2 + b^2 == c^2]

-- fibonacci
add [] []         = []
add (a:as) (b:bs) = (a+b) : (add as bs)

fibonacci  =  1 : 1 : add fibonacci (tail fibonacci)

-- sieve primes
primes = sieve [2..]
    where sieve (p:ps) = p : sieve [ x | x <- ps, x `mod` p /= 0 ]

-- quickSort
quickSort [] = []
quickSort (x:xs) = quickSort [ y | y <- xs, y <= x ]
                   ++ [x] ++
                   quickSort [ y | y <- xs, y > x ]



