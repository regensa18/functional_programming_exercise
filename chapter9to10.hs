-- length using map and sum
toOne _ = 1
length xs = sum (map toOne xs)

-- iter
iter :: Int -> (a -> a) -> (a -> a)
iter 0 f x = x
iter n f x = f (iter (n-1) f x)

-- square of natural numbers from 1 to n
sumOfSquare :: Integer -> Integer
sumOfSquare n = foldr (+) 0 $ map (\x -> x*x) [1..n]

mystery xs = foldr (++) [] (map sing xs)
 where
  sing x = [x]

-- mystery = memasukkan nilai xs ke list kosong inisial

-- composeList

composeList :: [a -> a] -> (a -> a)
composeList []     = id
composeList (f:fs) = f . composeList fs

-- number 2
number1       = map (+1)
number2 xs ys = concat (map (\x -> map (\y -> x+y) ys) xs)
number3 xs    = map (+2) (filter (>3) xs)
number4 xys   = map (\(x,_) -> x+3) xys

-- list comprehension
number1' xs    = [ x+3 | x <- xs ]
number2' xs    = [ x | x <- xs, x > 7]
number3' xs ys = [ (x,y) | x <- xs, y <- ys ]
number4' xys   = [ x+y | (x,y) <- xys, x+y > 3 ]
