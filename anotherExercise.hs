import Data.List

-- mergeSort
merge :: Ord a => [a] -> [a] -> [a]
merge xs [] = xs
merge [] ys = ys
merge (x:xs) (y:ys) | x <= y = x : merge xs (y:ys)
                    | otherwise = y : merge (x:xs) ys

msort :: Ord a => [a] -> [a]
msort [] = []
msort [a] = [a]
msort xs = merge (msort (firstHalf xs)) (msort (secondHalf xs))

firstHalf  xs = let { n = length xs } in take (div n 2) xs
secondHalf xs = let { n = length xs } in drop (div n 2) xs

-- kpk
kpk a b | a `mod` b == 0 = a
        | b `mod` a == 0 = b
        | b == a         = b
        | b /= a         = b * a

-- kpk revised
kpkNew a b = [ x | x <- [a, a*2 .. ], x `mod` b == 0 ] !! 0

-- kpk with map
kpkMap a b = [x | x <- (map (a*) [1..]), x `mod` b == 0 ] !! 0

-- hamming
mergeHam xxs@(x:xs) yys@(y:ys) | x == y = x : mergeHam xs ys
                            | x < y  = x : mergeHam xs yys
                            | x > y  = y : mergeHam xxs ys
hamming = 1 : mergeHam (map (2*) hamming)
                    (mergeHam (map (3*) hamming)
                            (map (5*) hamming))

-- merge sort from class
-- define merge first

mergeNew kiri@(x:xs) kanan@(y:ys) | x < y = x : merge xs kanan
                                  | y <= x = y : merge kiri ys

mergeSort xs = mergeNew (mergeSort kiri) (mergeSort kanan)
        where kiri = take ((length xs) `div` 2) xs
              kanan = drop ((length xs) `div` 2) xs

-- maxlist with recursive
maxList [] = 0
maxList (x:xs) = max x (maxList xs)

-- maxList with fold
maxListFold xs = foldl max 0 xs

-- permutations
perm [] = [[]]
perm ls = [ x:ps | x <- ls, ps <- perm(ls \\ [x])]

-- number 5 from quiz
misteri xs ys = concat (map (\x -> map (\y -> (x, y)) ys) xs)

-- number 5 with monad form

misteri1 = do
           x <- [1,2,3]
           y <- [4,5]
           return (x,y)

misteri2 = ([1,2,3] >>= \x -> [4,5] >>= \y -> return (x,y))

-- fpb
fpb x 0 = x
fpb x y = fpb (x `mod` y)

-- flip
myFlip :: (a -> b -> c) -> b -> a -> c
myFlip f a b = f b a